/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo;

/**
 *
 * @author emoru
 */
public class Recibo {
    private int numRecibo;
    private String fecha;
    private String nombre;
    private String domicilio;
    private int tipoServicio;
    private double costoKilowatts;
    private int consumoKilowatts;

    public Recibo() {
        // Constructor vacío
    }
        //Constructor de copia
    public Recibo(Recibo recibo) {
        // Constructor de copia
        this.numRecibo = recibo.numRecibo;
        this.fecha = recibo.fecha;
        this.nombre = recibo.nombre;
        this.domicilio = recibo.domicilio;
        this.tipoServicio = recibo.tipoServicio;
        this.costoKilowatts = recibo.costoKilowatts;
        this.consumoKilowatts = recibo.consumoKilowatts;
    }

    //Gets y sets...
    public int getNumRecibo() {
        return numRecibo;
    }

    public void setNumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public int getTipoServicio() {
        return tipoServicio;
    }

    public void setTipoServicio(int tipoServicio) {
        this.tipoServicio = tipoServicio;
    }

    public double getCostoKilowatts() {
        return costoKilowatts;
    }

    public void setCostoKilowatts(double costoKilowatts) {
        this.costoKilowatts = costoKilowatts;
    }

    public int getConsumoKilowatts() {
        return consumoKilowatts;
    }

    public void setConsumoKilowatts(int consumoKilowatts) {
        this.consumoKilowatts = consumoKilowatts;
    }

    public double calcularSubtotal() {
        return costoKilowatts * consumoKilowatts;
    }

    public double calcularImpuesto() {
        return calcularSubtotal() * 0.16;
    }

    public double calcularTotal() {
        return calcularSubtotal() + calcularImpuesto();
    }

    public void setTotal(float parseFloat) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public void setImpuesto(float parseFloat) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public void setSubTotal(float parseFloat) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public Object calcularSubTotal() {
return costoKilowatts * consumoKilowatts;
    }

    public Object consumoKilowatts() {
        this.consumoKilowatts = consumoKilowatts;
        return null;
        
    }

}

