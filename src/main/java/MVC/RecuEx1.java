/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package MVC;

import controlador.Controlador;
import javax.swing.JFrame;
import modelo.Recibo;
import vista.dlgRecibo;

/**
 *
 * @author emoru
 */
public class RecuEx1 {

        public static void main(String[] args) {
        // Crear objetos del modelo, vista y controlador
        Recibo recibo = new Recibo ();
        dlgRecibo vista = new dlgRecibo(new JFrame(),true);
        Controlador contra = new Controlador(recibo, vista);
        
        // Iniciar la vista
        contra.iniciarVista();
    }
}
