/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador;

/**
 *
 * @author emoru
 */
import modelo.Recibo;
import vista.dlgRecibo;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Controlador implements ActionListener {
    
    private Recibo recibo;
    private dlgRecibo vista;
    
    
    public Controlador(Recibo recibo, dlgRecibo vista){
        this.recibo = recibo;
        this.vista = vista;
        // Configurar los botones de la vista para que el controlador los escuche
        vista.btnCancelar.addActionListener(this);
        vista.btnCerrar.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
        vista.bntNuevo.addActionListener(this);
        vista.btnGuardar.addActionListener(this);
        vista.btnMostrar.addActionListener(this);
    }
    
    public void iniciarVista() {
        vista.setTitle(":: Productos ::");
        vista.setSize(500, 500);
        vista.setVisible(true);
    }
        
    @Override
    public void actionPerformed (ActionEvent e){
        if (e.getSource() == vista.btnLimpiar) {
            // Limpiar los campos de entrada en la vista
            vista.txtNumRecibo.setText("");
            vista.txtFecha.setText("");
            vista.txtNombre.setText("");
            vista.txtDomicilio.setText("");
            vista.txtTotal.setText("");
            vista.txtconsumoKilowatts.setText("");
            vista.txtcostoKilowatts.setText("");
            vista.txtimpuesto.setText("");
            vista.txtsubTotal.setText("");
        }
        
        if (e.getSource() == vista.bntNuevo) {
            // Habilitar los campos de entrada en la vista
            vista.txtNumRecibo.setEnabled(true); 
            vista.txtFecha.setEnabled(true); 
            vista.txtNombre.setEnabled(true); 
            vista.txtDomicilio.setEnabled(true); 
            vista.txtTotal.setEnabled(true); 
            vista.txtconsumoKilowatts.setEnabled(true); 
            vista.txtcostoKilowatts.setEnabled(true); 
            vista.txtimpuesto.setEnabled(true); 
            vista.txtsubTotal.setEnabled(true); 
        }
        
        if (e.getSource() == vista.btnGuardar) {
    // Obtener los valores de los campos de entrada y establecerlos en el modelo Recibo
        String numReciboStr = vista.txtNumRecibo.getText();
    try {
        int numRecibo = Integer.parseInt(numReciboStr);
        recibo.setNumRecibo(numRecibo);
    } catch (NumberFormatException ex) {
        JOptionPane.showMessageDialog(vista, "Error al ingresar el número de recibo: " + ex.getMessage());
    }

    recibo.setFecha(vista.txtFecha.getText());
    recibo.setNombre(vista.txtNombre.getText());
    recibo.setDomicilio(vista.txtDomicilio.getText());
    
    try {
        // Convertir los valores a tipos numéricos
        recibo.setTotal(Float.parseFloat(vista.txtTotal.getText()));
        recibo.setConsumoKilowatts((int) Float.parseFloat(vista.txtconsumoKilowatts.getText()));
        recibo.setCostoKilowatts(Float.parseFloat(vista.txtcostoKilowatts.getText()));
        recibo.setImpuesto(Float.parseFloat(vista.txtimpuesto.getText()));
        recibo.setSubTotal(Float.parseFloat(vista.txtsubTotal.getText()));
        
        // Realizar alguna acción adicional, como guardar el recibo en una base de datos o en una estructura de datos
        
        JOptionPane.showMessageDialog(vista, "Recibo guardado exitosamente");
    } catch (NumberFormatException ex) {
        JOptionPane.showMessageDialog(vista, "Error al guardar el recibo: " + ex.getMessage());
    } catch (Exception ex2) {
        JOptionPane.showMessageDialog(vista, "Error al guardar el recibo: " + ex2.getMessage());
    }
}

        
        if (e.getSource() == vista.btnMostrar) {
    // Mostrar los valores del modelo Recibo en los campos de la vista
    vista.txtNumRecibo.setText(String.valueOf(recibo.getNumRecibo()));
    vista.txtFecha.setText(recibo.getFecha());
    vista.txtNombre.setText(recibo.getNombre());
    vista.txtDomicilio.setText(recibo.getDomicilio());
    
    // Calcular y mostrar los totales relacionados con el recibo
    vista.txtTotal.setText(String.valueOf(recibo.calcularTotal()));
    vista.txtconsumoKilowatts.setText(String.valueOf(recibo.consumoKilowatts()));
    vista.txtcostoKilowatts.setText(String.valueOf(recibo.getCostoKilowatts()));
    vista.txtimpuesto.setText(String.valueOf(recibo.calcularImpuesto()));
    vista.txtsubTotal.setText(String.valueOf(recibo.calcularSubTotal()));
}

if (e.getSource() == vista.btnCerrar) {
    // Mostrar un cuadro de diálogo de confirmación para cerrar la ventana
    int option = JOptionPane.showConfirmDialog(vista, "¿Seguro que quieres salir?", "Confirmar salida", JOptionPane.YES_NO_OPTION);
            
    if (option == JOptionPane.YES_OPTION) {
        vista.dispose();
        System.exit(0);
    }
}

        
        if (e.getSource() == vista.btnCerrar) {
            // Cerrar la ventana/dialogo
            vista.dispose();
        }
        
        if (e.getSource() == vista.btnCancelar) {
            // Deshabilitar los campos de entrada en la vista
            vista.txtNumRecibo.setEnabled(false); 
            vista.txtFecha.setEnabled(false); 
            vista.txtNombre.setEnabled(false); 
            vista.txtDomicilio.setEnabled(false); 
            vista.txtTotal.setEnabled(false); 
            vista.txtconsumoKilowatts.setEnabled(false); 
            vista.txtcostoKilowatts.setEnabled(false); 
            vista.txtimpuesto.setEnabled(false); 
            vista.txtsubTotal.setEnabled(false); 
            
            // Limpiar los campos de entrada en la vista
            vista.txtNumRecibo.setText("");
            vista.txtFecha.setText("");
            vista.txtNombre.setText("");
            vista.txtDomicilio.setText("");
            vista.txtTotal.setText("");
            vista.txtconsumoKilowatts.setText("");
            vista.txtcostoKilowatts.setText("");
            vista.txtimpuesto.setText("");
            vista.txtsubTotal.setText("");
        }
    }
}
